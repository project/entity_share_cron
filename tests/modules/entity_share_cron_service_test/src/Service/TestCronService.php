<?php

declare(strict_types = 1);

namespace Drupal\entity_share_cron_service_test\Service;

use Drupal\entity_share_cron\EntityShareCronService;

/**
 * Service that allows to reset processed pages counter.
 */
class TestCronService extends EntityShareCronService {

  /**
   * Clear the processed pages counter.
   *
   * This is useful if it is needed to emulate a multiple execution in one.
   */
  public function resetProcessedCounter(): void {
    $this->processedCounter = [];
  }

}
